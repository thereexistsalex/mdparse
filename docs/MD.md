# mRendering Modified Markdown

This document specifies the subset and superset of markdown used to markup file for mRendering. A whole mRendering document should be marked up in one text file from which a parser will construct the separate JSON files.

A document contains many nodes be it *section headings*, *paragraphs*, *lists*, *images*, or other text. Each node of the document should be separated from other nodes by at least one blank line before and after. Lines that are not separated by a blank line may be considered to be part of the same node. Some nodes, such as *paragraphs*, can contain styling such as **bold** or *italic* text or links. Style markings and links should be in-line.

## UID

Many nodes have UIDs. UIDs are generated using the UUIDv4 standard. UIDs can be specified using the optional parameters like {"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}, however it is suggested that allow the parser to generate the UIDs. Once a UID has been generated it is also suggested that it not be changed.

## Disabled and Hidden Defaults

Nodes can be marked inactive or hidden by default using the options hash. An example of a options hash marking a node hidden by default is {"hidden": true}. An example of a options hash marking a node inactive by default is {"inactive": true}. The values for inactive include true, false, and null for inherit.

## Regular Nodes

### Document Title

The **Document Title** should be marked by a $ symbol. If used it must be the first tag used in the document.

	$ Document Title

### Paragraphs

Paragraphs require no special marking. They should be separated by at least one empty line. Lines of text not separated by an empty line will be considered one paragraph.

	This is a paragraph

	Followed by a second paragraph

	This is a paragraph that
	is split onto two lines

### Sections

Sections are denoted by a set of \# symbols. The number of \#s denote the level of the section. One \# represents a top level section, two a second level section, three a third level section, and so on. The title of the section should follow the \#s on the same line. Sections will go until they reach another section of equal or higher level.

	# Sections
	## Sub Section
	### Sub Section

#### Nested Sections

By default sections are pulled onto their own page for display. However, they can be nested in the same page as their parent section by appending a < to the end of the section tag. For example, nesting two sections in a parent section's page looks like...

	# Sections
	##< Sub Section
	##< Sub Section

#### Collapsible and Collapsed Sections

Nested sections can be made collapsible by appending either + or - at the end of a nested section tag. A collapsible section can be collapsed and uncollapsed by the user. + denotes the section is collapsible and uncollapsed by default, while - denotes the section is collapsible and collapsed by defualt.

	# Sections
	##< Regular Nested Section
	##<+ Collapsible Nested Section (Defualt Uncollapsed)
	##<- Collapsible Nested Section (Defualt Collapsed)

#### Ending Section

Sections end anytime a section of equal or lower level is started. However, if you would like to end a section without starting a section of equal or lower level, the tag <\# where the is one or more \#s ends the last section with a level equal to the number of \#s. For example a document where you end a level 2 Sub-Section early might look like...

	# Sections
	## Sub-Section
	### Sub-Sub-Section
	<##
	### Sub-Sub-Section

### Horizontal Rules

Horizontal Rules are denoted with either a \!, followed by either 'horizontalRule' or 'hr'. For example...

	!horizontalRule

or...

	!hr

### Lists

There are two types of lists:
+ Regular (\-)
+ Numbered (1.)

Each list is denoted by a series of lines starting in either a \- or a number followed by a period (1.). There must be a space after the list item marker. List items must not be separated by extra lines, otherwise they will be considered separate lists.

#### Regular List

	- List item one
	- List item two
	- List item three

#### Numbered List

	1. List item one
	2. List item two
	3. List item three

### Images

Images are comprised of a \!, followed by the title in brackets, and then the image name in parenthesis. The title brackets can be left off if there is no image title. **Each image tag should be on one line.** All images are now embedded in the document. To following denotes one image...

	![Image Title](imageName.jpg)

Multiple images that are marked up next to each other form a gallery. For example the following forms a gallery of three images...

	![Image Title](imageName.jpg)
	![Image Title](imageName.jpg)
	![Image Title](imageName.jpg)

To denote a series of images that don't form a gallery add one line of whitespace between each image markdown. For example, these three images will not form a gallery because of the extra whitespace lines...

	![Image Title](imageName.jpg)

	![Image Title](imageName.jpg)

	![Image Title](imageName.jpg)

### Audio

Audio clips are comprised of a \!, followed by the letter 'a', followed by the title in brackets, and then the audio clip name or url in parenthesis. The title brackets can be left off if there is no audio title. **The audio tag should be on one line.**

	!a[Audio Clip Title](audioFile.mp3)
	!a[Audio Clip Title](https://api.soundcloud.com/tracks/193270658)

To embed a video from SoundCloud you must supply the tracks link with the audio file id. Click the share button and then embed tab. Within the embed html you will find the link needed that looks similar to...

	https://api.soundcloud.com/tracks/193270658

### Video

Video clips are comprised of a \!, followed by the letter 'v', followed by the title in brackets, and then the video clip name or a url from Vimeo or YouTube in parenthesis. The title brackets can be left off if there is no video title. **The video tag should be on one line.**

	!v[Video Clip Title](videoFile.mp4)
	!v[Video Clip Title](https://youtu.be/zSW56QKUtgI)
	!v[Video Clip Title](https://vimeo.com/11774747)
	!v[Video Clip Title](reeldx.com/1422)

To embed a video from YouTube, click the share button next to the video and copy the share link into the markdown document. The link should look similar to...

	https://youtu.be/zSW56QKUtgI

To embed a video from Vimeo click the share paper airplane on the video. You must then provide a link with the 8 digit video id in it. Most of the time the link in the "link" box will work. An example of a working link is...

	https://vimeo.com/11774747

However, sometimes the link in the linkbox doesn't have the 8 digit video id and so you must supply the url portion from the "embed" box. An example of that link is...

	https://player.vimeo.com/video/124292043

To embed a video from ReelDx find the video id. The video id can be found on the [Medvid.io](http://medvid.io) website and is **NOT** the case id. Append the video id to the end of "reeldx.com/" and use that as the link. An example of a working link is...

	reeldx.com/1422

### Extra Text

Extra test is marked by > tag. Extra text will not be displayed in the documents and can be used for comments or to save text for other uses.

	> This is extra text

## Action Nodes
### Button
Buttons are nodes that perform actions when clicked. The markdown for buttons is a \!, followed by the letter 'b', followed by the title in brackets. Finally the options hash includes the UID as well as 'onClick' which contains the actions array. **The button tag should be on one line.**

	!b[Button 1]

	!b[Button 2]{"onClick": [{ "type" : "action", "action" : "setNodeInactive", "arguments" : {"uid": "uid", "value": 2}}]}

## Input Nodes
### Text Input

A text input node is marked with !textinput or !ti. Text input also has several extra parameters that can be included in {}

	!textInput{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "characterLimit": 100, "boxLines": 3, "validationType": "email", "regex": "regex"}
	!ti{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "characterLimit": 100, "boxLines": 3, "regex": "regex"}

#### Optional Parameters
- characterLimit : Number of characters allowed in response
- boxLines : Response box height in lines
- validationType : Pre defined validation types like email or alphanumeric
- regularExpression : Regular expression for validation if there is no validation type

#### Default Parameters
If no values are present for traits then the default is...
- characterLimit : None
- boxLines : 1
- validationType : None
- regularExpression : None

### Multiple Choice

A multiple choice question is marked with !N and !Y...

	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit

For multiple choice questions that allow user to select multiple boxes use options hash...

	{"multipleSelect" : true}
	!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit

With UIDs...

	{"uid": "66533c30-a869-45c6-83c4-73809f6a420e", "multipleSelect" : true}
	!N{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N{"uid": "3be110fb-cde4-4be6-ac85-8eb03ac70796"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y{"uid": "5652c538-22e9-46b0-806a-641ea0281eb0"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N{"uid": "ac1aca7e-4333-4016-ac4e-be3b0734a9c5"} Lorem ipsum dolor sit amet, consectetur adipiscing elit

### Key Feature

A key feature question is marked with !Y, !N and !P to denote correct, incorrect and poison answers choices respectively. The key feature group number is places right after the !Y, !N or !P marker...

	!Y1 Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N1 Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y2 Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N2 Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!P1 Lorem ipsum dolor sit amet, consectetur adipiscing elit

With UIDs...

	{"uid": "66533c30-a869-45c6-83c4-73809f6a420e", "limit": 10, "isExplicitLimit": true}
	!Y1{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N1{"uid": "3be110fb-cde4-4be6-ac85-8eb03ac70796"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y2{"uid": "5652c538-22e9-46b0-806a-641ea0281eb0"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!P1{"uid": "ac1aca7e-4333-4016-ac4e-be3b0734a9c5"} Lorem ipsum dolor sit amet, consectetur adipiscing elit

#### Optional Parameters
	- limit : The number of answers allowed
	- isExplicitLimit : If the number of answers is explicitly enforced

### Slider Bar

A slider bar is marked as !sliderbar with optional parameters in {}

	!sliderBar{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "minValue": 10, "maxValue": 90, "step": 10, "showValue": true}

#### Optional Parameters
- minValue : The minimum value of the slider
- maxValue : The maximum value of the slider
- step : The step size of the slider
- showValue : Dictates if user should see value

#### Default Parameters
If no values are present for traits then the default is...
- minValue : 0
- maxValue : 100
- step : 1
- showValue : False

### Rating Bar

A rating bar is marked at !ratingbar with optional parameters in {}

	!ratingBar{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "maxValue": 10}

#### Optional Parameters
- maxValue : The maximum value of the slider

#### Default Parameters
If no values are present for traits then the default is...
- maxValue : 5

### Checkbox
A checkbox is marked by + followed by text. The checkbox replaces the checklist

	+ Lorem ipsum dolor sit amet, consectetur adipiscing elit

With UID...

	+{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit

### Matcher
A matcher is marked by pairs of !A and !B where successive !A !B pairs are matches

	!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B Lorem ipsum dolor sit amet, consectetur adipiscing elit

with UIDs...

	{"uid": "66533c30-a869-45c6-83c4-73809f6a420e"}
	!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit

## Style Marking

### Italic

Italics are marked by one leading and trailing asterisk

	This sentence has *two words* that are italicized

### Bold

Bold text is marked by two leading and trailing asterisks

	This sentence has **two words** that are bold

### Underline

Underline text is marked by three leading and trailing asterisks

	This sentence has ***two words*** that are underlined

### Superscript

Superscript text is marked by a leading and trailing caret character (^)

	This sentence contains the equation E=MC^2^ with a superscript 2

### Subscript

Subscript text is marked by a leading and trailing tilde character (~)

	This sentence contains the chemical name CO~2~ with a subscript 2


### Links

Links are comprised of the title in brackets and then the hyper-link in parenthesis

	[Link Text](www.link.com)

In addition to http URLs links can also contain mailto: and tel: URIs.

	[Send an email](mailto:email@test.com)
	[Make a phone call](tel:15555555555)

## Special JSON Insert

To insert custom JSON into the document use the `!json` before the custom JSON and `<!json` after.

	!json
	{
		"type": "other",
		"array": [
			"test1",
			"test2",
			"test3"
		],
		"object": {
			"type": "test4"
		}
	}
	<!json

## Example Document

	$ Document Title

	# Section Title

	Lorem *italic text* ipsum dolor sit amet, consectetur adipisicing elit,
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
	enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
	aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
	in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
	sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
	mollit anim id est laborum.

	## Sub Section Title

	Lorem **bold text** ipsum dolor sit amet, consectetur adipisicing elit,
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
	enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
	aliquip ex ea commodo consequat.

	###<+ Collapsible Nested Sub Sub Section

	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	tempor incididunt ut labore et dolore magna aliqua.

	## Second Sub Section Title

	Lorem [link](http://link) amet, consectetur adipisicing elit, sed do
	eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
	minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
	ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
	voluptate velit esse cillum dolore eu fugiat nulla pariatur.

	## Third Sub Section

	![image](imageName)

	-List Item One
	-List Item Two
	-List Item Three

	#{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Everything

	{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} This is a paragraph

	{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}
	- List item one
	- List item two
	- List item three

	{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}
	1. List item one
	2. List item two
	3. List item three

	![Image Title](imageName.jpg){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}

	{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}
	![Image Gallery Title 1](imageName.jpg){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}
	![Image Gallery Title 2](imageName.jpg){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}
	![Image Gallery Title 3](imageName.jpg){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}

	!a[Audio Clip Title](audioFile.mp3){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}

	!a[Audio Clip Title](https://api.soundcloud.com/tracks/193270658){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}

	!v[Video Clip Title](videoFile.mp4){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}

	!v[Video Clip Title](https://youtu.be/zSW56QKUtgI){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}

	!v[Video Clip Title](https://vimeo.com/11774747){"uid": "e7736398-b473-43e0-9626-e8d2651b8408"}

	> This is extra text

	!textInput{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "characterLimit": 100, "boxLines": 3, "validationType": "email", "regex": "regex"}

	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit

	{"multipleSelect" : true}
	!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N Lorem ipsum dolor sit amet, consectetur adipiscing elit

	{"uid": "66533c30-a869-45c6-83c4-73809f6a420e", "multipleSelect" : true}
	!N{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N{"uid": "3be110fb-cde4-4be6-ac85-8eb03ac70796"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!Y{"uid": "5652c538-22e9-46b0-806a-641ea0281eb0"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!N{"uid": "ac1aca7e-4333-4016-ac4e-be3b0734a9c5"} Lorem ipsum dolor sit amet, consectetur adipiscing elit

	!sliderBar{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "minValue": 10, "maxValue": 90, "step": 10, "showValue": true}

	!ratingBar{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "maxValue": 10}

	+ Lorem ipsum dolor sit amet, consectetur adipiscing elit

	+{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit

	!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B Lorem ipsum dolor sit amet, consectetur adipiscing elit

	{"uid": "66533c30-a869-45c6-83c4-73809f6a420e"}
	!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
	!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
