require './lib/mdparse'
require 'set'

def root_section_wrapper(content)
	{type: :section, uid: "uid", title: "", level: 0, content: content, nested: false, collapsible: false, collapsed: false}
end

def assert_md_matches_generated_md(md)
	json_a = MDParse::Document.parse(md)
	md_a = json_a.to_md
	json_b = MDParse::Document.parse(md_a)
	md_b = json_b.to_md
	json_c = MDParse::Document.parse(md_b)

	expect(json_c.to_h).to eq(json_a.to_h)
end

describe "MDParse" do
	before(:each) do
		allow(SecureRandom).to receive(:uuid).and_return("uid")

		@uid_string = "{\"uid\":\"uid\"}"

		@texts = ["Lorem ipsum dolor sit amet",
				"consectetur adipisicing elit, sed",
				"eiusmod tempor incididunt ut labore",
				"et dolore magna aliqua. Ut enim ad minim veniam",
				"quis nostrud exercitation ullamco laboris nisi",
				"ut aliquip ex ea commodo consequat. Duis aute",
				"irure dolor in reprehenderit in voluptate velit",
				"esse cillum dolore eu fugiat nulla pariatur.",
				"Excepteur sint occaecat cupidatat non proident,",
				"sunt in culpa qui officia deserunt mollit anim id est laborum."]
	end

	describe MDParse::Section do
		before(:each) do
			@titles = (0..6).map{|i| "Title " + i.to_s}
			@levels = (0..6).map{|i| i % 3 + 1}

			@sections = (0..6).map{|i| MDParse::Section.new("   " + @titles[i], @levels[i])}
		end

		describe ".initialize" do
			it "should set the correct original title and level" do
				expect(@sections[0].original_title).to eq(@titles[0])
				expect(@sections[0].level).to eq(@levels[0])
			end
		end

		describe ".title" do
			it "should return an array with correct title" do
				expect(@sections[0].title).to eq(@titles[0])
			end

			[["italic", "*", "em"], ["bold", "**", "strong"], ["underlined", "***", "u"], ["superscript", "^", "sup"], ["subscript", "~", "sub"]].each do |style, marking, html_tag|
				it "should return " + style + " text when it contains " + style + " text" do
					section = MDParse::Section.new(marking + @titles[0] + marking, 1)
					expect(section.to_h).to eq({type: :section, uid: "uid", title: "<#{html_tag}>" + @titles[0]+ "</#{html_tag}>", level: 1, content: [], nested: false, collapsible: false, collapsed: false})
				end
			end
		end

		describe ".to_h" do
			it "should return a hash with correct type, title, level, content" do
				@sections[0] << @sections[1]

				expect(@sections[0].to_h).to eq({type: :section, uid: "uid", title: @titles[0], level: @levels[0], content: [
												{type: :section, uid: "uid", title: @titles[1], level: @levels[1], content: [], nested: false, collapsible: false, collapsed: false}
											], nested: false, collapsible: false, collapsed: false})
			end
		end

		describe ".to_md" do
			it "generates a first level section" do
				sec = "#" + @texts[0]
				expect(MDParse::Document.parse(MDParse::Document.parse(sec).to_md).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "generates an n level section" do
				n = 20
				sec = "#" * n + " " * 14 + @texts[0]
				expect(MDParse::Document.parse(MDParse::Document.parse(sec).to_md).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: n, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "generates a multiline section" do
				sec =  "#" + " " + @texts[0] + "   \n" + @texts[1]
				section_title = @texts[0] + " " + @texts[1]

				expect(MDParse::Document.parse(MDParse::Document.parse(sec).to_md).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: section_title, level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "generates a nested section" do
				sec = "#<" + " "+ @texts[0]
				expect(MDParse::Document.parse(MDParse::Document.parse(sec).to_md).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: true, collapsible: false, collapsed: false}]))
			end

			it "generates a nested collapsible collapsed section" do
				sec = "#<-" + " "+ @texts[0]
				expect(MDParse::Document.parse(MDParse::Document.parse(sec).to_md).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: true, collapsible: true, collapsed: true}]))
			end

			it "generates a nested collapsible not collapsed section" do
				sec = "#<+" + " "+ @texts[0]
				expect(MDParse::Document.parse(MDParse::Document.parse(sec).to_md).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: true, collapsible: true, collapsed: false}]))
			end
		end

		describe ".add_line" do
			it "should add the line to the title without whitespace" do
				@sections[0].add_line("    " + @titles[1] + "    ")

				expect(@sections[0].original_title).to eq(@titles[0] + " " + @titles[1])
				expect(@sections[0].title).to eq(@titles[0] + " " + @titles[1])
			end
		end

		describe ".child_sections" do
			it "should return an array of child sections" do
				@sections[0] << @sections[1] << @sections[2] << @sections[3]

				expect(@sections[0].child_sections).to eq([@sections[1], @sections[2], @sections[3]])
			end
		end

		describe ".decendent_sections" do
			it "should return an array of child sections" do
				@sections[1] << @sections[2]
				@sections[0] << @sections[1] << @sections[3]

				expect(@sections[0].decendent_sections).to eq([@sections[1], @sections[2], @sections[3]])
			end
		end
	end

	describe MDParse::ContentSection do
		before(:each) do
			@conSecs = (0..6).map{|i| MDParse::ContentSection.new()}
			@conSecs[0] << @conSecs [1]
			@conSecs[0] << @conSecs [2]
			@conSecs[2] << @conSecs [3]
			@conSecs[3] << @conSecs [4]
			@conSecs[3] << @conSecs [5]
			@conSecs[3] << @conSecs [6]
			@conSecs[6] << "Random"
			@conSecs[2] << "More Random"
		end

		# describe ".content_inject" do
		# 	it "should hit all nodes" do
		# 		finAcc = @conSecs[0].content_inject(0) do |acc, conSec|
		# 			acc + 1
		# 		end
		#
		# 		expect(finAcc).to eq(@conSecs.length + 2)
		# 	end
		# end
		#
		# describe ".content_flatten" do
		# 	it "should return array containing all the nodes" do
		# 		flatArray = @conSecs[0].content_flatten
		#
		# 		expect(Set.new(flatArray)).to eq(Set.new(@conSecs + ["Random", "More Random"]))
		# 	end
		# end
		#
		# describe ".content_select" do
		# 	it "should return tree with only selected nodes" do
		# 		selectedCons = @conSecs[0].content_select{|node| node.class == MDParse::ContentSection}
		# 		expect(selectedCons.content_flatten.length).to eq(@conSecs.length)
		# 	end
		# end
	end

	describe MDParse::Paragraph do
		before(:each) do
			@lines = (0..6).map{|i| "Lorem ipsum " + i.to_s}

			@paragraphs = (0..6).map{|i| MDParse::Paragraph.new(@lines[i])}
		end

		describe ".initialize" do
			it "should set the correct original text" do
				expect(@paragraphs[0].original_text).to eq(@lines[0])
			end
		end

		describe ".content" do
			it "should contain original text processed" do
				expect(@paragraphs[0].content).to eq(MDParse::FormattedText.parse(@lines[0]))
			end
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@paragraphs[0].to_h).to eq({type: :paragraph, uid: "uid",  content: MDParse::FormattedText.parse(@lines[0])})
			end

			[["italic", "*", "em"], ["bold", "**", "strong"], ["underlined", "***", "u"], ["superscript", "^", "sup"], ["subscript", "~", "sub"]].each do |style, marking, html_tag|
				it "should return " + style + " text when it contains " +  style + " text" do
					paragraph = MDParse::Paragraph.new(marking + @lines[0] + marking)
					expect(paragraph.to_h).to eq({type: :paragraph, uid: "uid",  content: "<#{html_tag}>#{@lines[0]}</#{html_tag}>"})
				end
			end
		end

		describe ".add_line" do
			it "should add the line to the original text" do
				@paragraphs[0].add_line("    " + @lines[1] + "    ")

				expect(@paragraphs[0].original_text).to eq(@lines[0] + " " + @lines[1])
			end

			it "should add the line to the parsed content" do
				@paragraphs[0].add_line("    " + @lines[1] + "    ")

				expect(@paragraphs[0].content).to eq((MDParse::FormattedText.parse(@lines[0] + " " + @lines[1])))
			end

			it "should connect lines with dashes" do
				@paragraphs[0].add_line(@lines[1] + "-")
				@paragraphs[0].add_line(@lines[2])

				expect(@paragraphs[0].content).to eq((MDParse::FormattedText.parse(@lines[0] + " " + @lines[1] + @lines[2])))
			end

			describe ".to_md" do
				it "should return string with the proper format" do
					expect(@paragraphs[0].to_md).to eq("#{@uid_string} #{@lines[0]}")
				end
			end
		end
	end

	describe MDParse::Extra do
		before(:each) do
			@lines = (0..6).map{|i| "Lorem ipsum " + i.to_s}

			@extras = (0..6).map{|i| MDParse::Extra.new(@lines[i])}
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@extras[0].to_h).to eq({type: :extra, uid: "uid",  content: MDParse::FormattedText.parse(@lines[0])})
			end

			[["italic", "*", "em"], ["bold", "**", "strong"], ["underlined", "***", "u"], ["superscript", "^", "sup"], ["subscript", "~", "sub"]].each do |style, marking, html_tag|
				it "should return " + style + " text when it contains " +  style + " text" do
					extra = MDParse::Extra.new(marking + @lines[0] + marking)
					expect(extra.to_h).to eq({type: :extra, uid: "uid",  content: "<#{html_tag}>#{@lines[0]}</#{html_tag}>"})
				end
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				expect(@extras[0].to_md).to eq(">#{@uid_string} #{@lines[0]}")
			end
		end
	end


	describe MDParse::RegularList do
		before(:each) do
			@lines = (0..6).map{|i| "Regular list line " + i.to_s}
			@regular_lists = (0..6).map do |i|
				rl = MDParse::RegularList.new()
				rl.add_item(@lines[i])
				rl
			end
		end

		describe ".intialize" do
			it "should set the correct original list item" do
				expect(@regular_lists[0].content).to eq([MDParse::ListItem.new(@lines[0])])
			end
		end

		describe ".add_line" do
			it "should add line to the last list item" do
				@regular_lists[0].add_line(@lines[1])

				listItem = MDParse::ListItem.new(@lines[0])
				listItem.add_line(@lines[1])

				expect(@regular_lists[0].content).to eq([listItem])
			end
		end

		describe ".add_item" do
			it "should add another line" do
				@regular_lists[0].add_item(@lines[1])

				listItem0 = MDParse::ListItem.new(@lines[0])
				listItem1 = MDParse::ListItem.new(@lines[1])

				expect(@regular_lists[0].content).to eq([listItem0, listItem1])
			end
		end

		describe ".to_h" do
			it "should return hash with proper type and content" do
				expect(@regular_lists[0].to_h).to eq({type: :regularList, uid: "uid", content: @regular_lists[0].content.map{|x| x.to_h}})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@regular_lists[0].add_item(@lines[1])
				expect(@regular_lists[0].to_md).to eq("#{@uid_string}\n-#{@uid_string} #{@lines[0]}\n-#{@uid_string} #{@lines[1]}")
			end
		end
	end

	describe MDParse::Checklist do
		before(:each) do
			@lines = (0..6).map{|i| "Checklist line " + i.to_s}
			@checklists = (0..6).map do |i|
				c = MDParse::Checklist.new()
				c.add_item(@lines[i])
				c
			end
		end

		describe ".to_h" do
			it "should return hash with proper type and content" do
				expect(@checklists[0].to_h).to eq({type: :checklist, uid: "uid", content: @checklists[0].content.map{|x| x.to_h}})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@checklists[0].add_item(@lines[1])
				expect(@checklists[0].to_md).to eq("#{@uid_string}\n+#{@uid_string} #{@lines[0]}\n+#{@uid_string} #{@lines[1]}")
			end
		end
	end

	describe MDParse::NumberedList do
		before(:each) do
			@lines = (0..6).map{|i| "Numbered List line " + i.to_s}
			@numbered_lists = (0..6).map do |i|
				nl = MDParse::NumberedList.new()
				nl.add_item(@lines[i])
				nl
			end

		end

		describe ".to_h" do
			it "should return hash with proper type and content" do
				expect(@numbered_lists[0].to_h).to eq({type: :numberedList, uid: "uid", content: @numbered_lists[0].content.map{|x| x.to_h}})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@numbered_lists[0].add_item(@lines[1])
				expect(@numbered_lists[0].to_md).to eq("#{@uid_string}\n1.#{@uid_string} #{@lines[0]}\n2.#{@uid_string} #{@lines[1]}")
			end
		end
	end

	describe MDParse::ListItem do
		before(:each) do
			@lines = (0..6).map{|i| "Lorem ipsum " + i.to_s}
			@list_items = (0..6).map{|i| MDParse::ListItem.new(@lines[i])}
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@list_items[0].to_h).to eq({type: :listItem, uid: "uid",  content: MDParse::FormattedText.parse(@lines[0])})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				expect(@list_items[0].to_md).to eq("-#{@uid_string} #{@lines[0]}")
			end
		end
	end

	describe MDParse::HorizontalRule do
		before(:each) do
			@horizontal_rule = MDParse::HorizontalRule.new()

			@markdowns = [
				%$!horizontalRule$,
				%$!horizontalRule{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "classes": ["a special class"]}$
			]
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@horizontal_rule.to_h).to eq({type: :horizontalRule, uid: "uid"})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::Media do
		before(:each) do
			@titles = (0..6).map{|i| "Title " + i.to_s}
			@uris = (0..6).map{|i| "URI" + i.to_s + ".png"}

			@medias = (0..6).map{|i| MDParse::Media.new(@titles[i], @uris[i])}
		end

		describe ".initialize" do
			it "should set the correct title and uri" do
				expect(@medias[0].original_title).to eq(@titles[0])
				expect(@medias[0].uri).to eq(@uris[0])
				expect(@medias[0].source).to eq(:local)
			end
		end

		describe ".to_h" do
			it "should return a hash with correct type, title and uri" do
				expect(@medias[0].to_h).to eq({type: :media, uid: "uid", title: @titles[0], source: :local, uri: @uris[0]})
			end
		end

		describe ".title" do
			it "should return an array with correct title" do
				expect(@medias[0].title).to eq(@titles[0])
			end

			[["italic", "*", "em"], ["bold", "**", "strong"], ["underlined", "***", "u"], ["superscript", "^", "sup"], ["subscript", "~", "sub"]].each do |style, marking, html_tag|
				it "should return " + style + " text when it contains " + style + " text" do
					media = MDParse::Media.new(marking + @titles[0] + marking, @uris[0])
					expect(media.to_h).to eq({type: :media, uid: "uid",  title: "<#{html_tag}>" + @titles[0]+ "</#{html_tag}>", source: :local, uri: @uris[0]})
				end
			end
		end
	end

	describe MDParse::Image do
		before(:each) do
			@titles = (0..6).map{|i| "Title " + i.to_s}
			@file_names = (0..6).map{|i| "FileName" + i.to_s + ".png"}

			@images = (0..6).map{|i| MDParse::Image.new(@titles[i], @file_names[i])}
		end

		describe ".initialize" do
			it "should set the correct title and file name" do
				expect(@images[0].original_title).to eq(@titles[0])
				expect(@images[0].uri).to eq(@file_names[0])
			end
		end

		describe ".to_h" do
			it "should return a hash with correct type, title and file name" do
				expect(@images[0].to_h).to eq({type: :image, uid: "uid", title: @titles[0], uri: @file_names[0], source: :local})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				expect(@images[0].to_md).to eq("!i[#{@images[0].title}](#{@images[0].uri})#{@uid_string}")
			end
		end
	end

	describe MDParse::ImageGallery do
		before(:each) do
			@titles = (0..6).map{|i| "Title " + i.to_s}
			@file_names = (0..6).map{|i| "FileName" + i.to_s + ".png"}

			@images = (0..6).map{|i| MDParse::Image.new(@titles[i], @file_names[i])}
			@image_gallery = MDParse::ImageGallery.new(@images[0])
		end

		describe ".images" do
			it "should return an array of images in the gallery" do
				expect(@image_gallery.images).to eq([@images[0]])
			end
		end

		describe ".add_image" do
			it "should add image to array of images in the gallery" do
				@image_gallery.add_image(@images[1])
				expect(@image_gallery.images).to eq([@images[0], @images[1]])
			end
		end

		describe ".to_h" do
			it "should return a image gallery with correct images when two or more images in set" do
				@image_gallery.add_image(@images[1])
				expect(@image_gallery.to_h).to eq({type: :imageGallery, uid: "uid", images: [{type: :image, uid: "uid", title: @titles[0], uri: @file_names[0], source: :local}, {type: :image, uid: "uid", title: @titles[1], uri: @file_names[1], source: :local}]})
			end

			it "should return only an image when there is just one image" do
				expect(@image_gallery.to_h).to eq({type: :image, uid: "uid", title: @titles[0], uri: @file_names[0], source: :local})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format when two or more images in set" do
				@image_gallery.add_image(@images[1])
				expect(@image_gallery.to_md).to eq(@uid_string + "\n" + @image_gallery.images.map{|x| x.to_md}.join("\n"))
			end

			it "should return string with the proper format when there is just one image" do
				expect(@image_gallery.to_md).to eq(@image_gallery.images.first.to_md)
			end
		end
	end

	describe MDParse::Video do
		before(:each) do
			@titles = (0..6).map{|i| "Title " + i.to_s}
			@uris = (0..6).map{|i| "URI" + i.to_s + ".png"}

			@videos = (0..6).map{|i| MDParse::Video.new(@titles[i], @uris[i])}

			@youtube_uris = ["zSW56QKUtgI", "KWZGAExj-es", "zSW56QKUtgI"]
			@youtube_bases = ["https://youtu.be/"]
			@youtube_video = MDParse::Video.new(@titles[0], @youtube_bases[0] + @youtube_uris[0])

			@vimeo_uris = ["12345678"]
			@vimeo_bases = ["https://vimeo.com/",
			 								"https://vimeo.com/channels/staffpicks/",
			 								"https://player.vimeo.com/video/"]
			@vimeo_video = MDParse::Video.new(@titles[0], @vimeo_bases[0] + @vimeo_uris[0])

			@reeldx_uris = ["1422"]
			@reeldx_bases = ["reeldx.com/",
											 "https://reeldx.com/"]
			@reeldx_video = MDParse::Video.new(@titles[0], @reeldx_bases[0] + @reeldx_uris[0])
		end

		describe ".initialize" do
			it "should set the correct title, uri, and source" do
				expect(@videos[0].original_title).to eq(@titles[0])
				expect(@videos[0].uri).to eq(@uris[0])
				expect(@videos[0].source).to eq(:local)
			end

			it "should set the correct title, uri, and source for YouTube videos" do
				@youtube_uris.product(@youtube_bases).each do |uri, base|
					video = MDParse::Video.new(@titles[0], base + uri)
					expect(video.original_title).to eq(@titles[0])
					expect(video.uri).to eq(uri)
					expect(video.source).to eq(:youtube)
				end
			end

			it "should set the correct title, uri, and source for Vimeo videos" do
				@vimeo_uris.product(@vimeo_bases).each do |uri, base|
					video = MDParse::Video.new(@titles[0], base + uri)
					expect(video.original_title).to eq(@titles[0])
					expect(video.uri).to eq(uri)
					expect(video.source).to eq(:vimeo)
				end
			end

			it "should set the correct title, uri, and source for ReelDx videos" do
				@reeldx_uris.product(@reeldx_bases).each do |uri, base|
					video = MDParse::Video.new(@titles[0], base + uri)
					expect(video.original_title).to eq(@titles[0])
					expect(video.uri).to eq(uri)
					expect(video.source).to eq(:reeldx)
				end
			end
		end

		describe ".to_h" do
			it "should return a hash with correct type, title and uri" do
				expect(@videos[0].to_h).to eq({type: :video, uid: "uid", title: @titles[0], source: :local, uri: @uris[0]})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				expect(@videos[0].to_md).to eq("!v[#{@videos[0].title}](#{@videos[0].uri})#{@uid_string}")
				expect(@youtube_video.to_md).to eq("!v[#{@youtube_video.title}](youtu.be/#{@youtube_video.uri})#{@uid_string}")
				expect(@vimeo_video.to_md).to eq("!v[#{@vimeo_video.title}](vimeo.com/#{@vimeo_video.uri})#{@uid_string}")
				expect(@reeldx_video.to_md).to eq("!v[#{@reeldx_video.title}](reeldx.com/#{@reeldx_video.uri})#{@uid_string}")
			end
		end
	end

	describe MDParse::Audio do
		before(:each) do
			@titles = (0..6).map{|i| "Title " + i.to_s}
			@uris = (0..6).map{|i| "URI" + i.to_s + ".png"}

			@audios = (0..6).map{|i| MDParse::Audio.new(@titles[i], @uris[i])}

			@soundcloud_uri = "193270658"
			@soundcloud_audio = MDParse::Audio.new(@titles[0], "https://api.soundcloud.com/tracks/" + @soundcloud_uri)
		end

		describe ".initialize" do
			it "should set the correct title, uri, and source" do
				expect(@audios[0].original_title).to eq(@titles[0])
				expect(@audios[0].uri).to eq(@uris[0])
				expect(@audios[0].source).to eq(:local)
			end

			it "should set the correct title, uri, and source for soundcloud" do
				expect(@soundcloud_audio.original_title).to eq(@titles[0])
				expect(@soundcloud_audio.uri).to eq(@soundcloud_uri)
				expect(@soundcloud_audio.source).to eq(:soundcloud)
			end
		end

		describe ".to_h" do
			it "should return a hash with correct type, title and uri" do
				expect(@audios[0].to_h).to eq({type: :audio, uid: "uid", title: @titles[0], source: :local, uri: @uris[0]})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				expect(@audios[0].to_md).to eq("!a[#{@audios[0].title}](#{@audios[0].uri})#{@uid_string}")
				expect(@soundcloud_audio.to_md).to eq("!a[#{@soundcloud_audio.title}](api.soundcloud.com/tracks/#{@soundcloud_audio.uri})#{@uid_string}")
			end
		end
	end

	describe MDParse::Link do
		before(:each) do
			@titles = (0..6).map{|i| "Title " + i.to_s}
			@urls = (0..6).map{|i| "http://" + "website" + i.to_s + ".com"}
			@mailto = "mailto:name@email.com"
			@tel = "tel:5555555555"
			@local_path = "/local/link"

			@links = (0..6).map{|i| MDParse::Link.new(@titles[i], @urls[i])}
			@link_mailto = MDParse::Link.new(@titles[0], @mailto)
			@link_tel = MDParse::Link.new(@titles[0], @tel)
			@local_link = MDParse::Link.new(@titles[0], @local_path)
		end

		describe ".initialize" do
			it "should set the correct title and file name" do
				expect(@links[0].original_title).to eq(@titles[0])
				expect(@links[0].url).to eq(@urls[0])
			end
		end

		describe ".to_h" do
			it "should return a hash with correct type, title and file name" do
				expect(@links[0].to_h).to eq("<a href=\"#{@urls[0]}\" target=\"_blank\">#{@titles[0]}</a>")
			end
		end

		describe ".url" do
			it "should return an url with http:// if missing" do
				url = "www.test.com"
				link = MDParse::Link.new(@titles[0], url)
				expect(link.url).to eq("http://" + url)
			end

			it "should not add http:// to url already containing it" do
				expect(@links[0].url).to eq(@urls[0])
			end

			it "should not add http:// when mail to url already containing it" do
				expect(@link_mailto.url).to eq(@mailto)
			end

			it "should not add http:// when tel url already containing it" do
				expect(@link_tel.url).to eq(@tel)
			end

			it "should allow a local link that starts with /" do
				expect(@local_link.url).to eq(@local_path)
			end
		end
	end

	describe MDParse::BoldText do
		before(:each) do
			@lines = (0..6).map{|i| "Lorem ipsum " + i.to_s}

			@bold_texts = (0..6).map{|i| MDParse::BoldText.new(@lines[i])}
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@bold_texts[0].to_h).to eq("<strong>" +  MDParse::FormattedText.parse(@lines[0]) + "</strong>")
			end
		end
	end

	describe MDParse::ItalicText do
		before(:each) do
			@lines = (0..6).map{|i| "Lorem ipsum " + i.to_s}

			@italic_texts = (0..6).map{|i| MDParse::ItalicText.new(@lines[i])}
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@italic_texts[0].to_h).to eq("<em>" + MDParse::FormattedText.parse(@lines[0]) + "</em>")
			end
		end
	end

	describe MDParse::UnderlinedText do
		before(:each) do
			@lines = (0..6).map{|i| "Lorem ipsum " + i.to_s}

			@underlined_texts = (0..6).map{|i| MDParse::UnderlinedText.new(@lines[i])}
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@underlined_texts[0].to_h).to eq("<u>" + MDParse::FormattedText.parse(@lines[0]) + "</u>")
			end
		end
	end

	describe MDParse::SuperscriptText do
		before(:each) do
			@lines = (0..6).map{|i| "Lorem ipsum " + i.to_s}

			@superscript_texts = (0..6).map{|i| MDParse::SuperscriptText.new(@lines[i])}
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@superscript_texts[0].to_h).to eq("<sup>" + MDParse::FormattedText.parse(@lines[0]) + "</sup>")
			end
		end
	end

	describe MDParse::JSONNode do
		before(:each) do
			@hash = {"type" => "test0", "array" => ["test1", "test2", "test3"], "object" => {"type" => "test4"}}
			@json = JSON.generate(@hash)

			@json_node = MDParse::JSONNode.new(@json)
		end

		describe ".to_h" do
			it "should return correct hash" do
				expect(@json_node.to_h).to eq(@hash)
			end
		end
	end

	describe MDParse::TextInput do
		before(:each) do
			@markdowns = [%$!textInput{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "characterLimit": 100, "boxLines": 3, "validationType": "email", "regex": "regex"}$]

			@default_text_input = MDParse::TextInput.new()
			@options = {characterLimit: 1000, boxLines: 5, validationType: "email"}
			@options_text_input = MDParse::TextInput.new(@options)
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@default_text_input.to_h).to eq({type: :textInput, uid: "uid", boxLines: 1, value: ""})
				expect(@options_text_input.to_h).to eq({type: :textInput, uid: "uid", characterLimit: 1000, boxLines: 5, validationType: "email", value: ""})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::MultipleChoice do
		before(:each) do
			@lines = (0..6).map{|i| "MultipleChoice line " + i.to_s}
			@multiple_choices = (0..6).map do |i|
				mc = MDParse::MultipleChoice.new(i.even? ? {multipleSelect: true} : {})
				mc.add_item(@lines[i], i.even?)
				mc
			end

			@markdowns = [
# "!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N Lorem ipsum dolor sit amet, consectetur adipiscing elit",

%${"multipleSelect": true}
!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
!N Lorem ipsum dolor sit amet, consectetur adipiscing elit$,

# %${"uid": "66533c30-a869-45c6-83c4-73809f6a420e"}
# !N{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N{"uid": "3be110fb-cde4-4be6-ac85-8eb03ac70796"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !Y{"uid": "5652c538-22e9-46b0-806a-641ea0281eb0"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N{"uid": "ac1aca7e-4333-4016-ac4e-be3b0734a9c5"} Lorem ipsum dolor sit amet, consectetur adipiscing elit$
			]
		end

		describe ".intialize" do
			it "should set the correct original list item" do
				expect(@multiple_choices[0].content).to eq([MDParse::MultipleChoiceAnswer.new(@lines[0], 0.even?)])
			end
		end

		describe ".add_line" do
			it "should add line to the last list item" do
				@multiple_choices[0].add_line(@lines[1])

				listItem = MDParse::MultipleChoiceAnswer.new(@lines[0], 0.even?)
				listItem.add_line(@lines[1])

				expect(@multiple_choices[0].content).to eq([listItem])
			end
		end

		describe ".add_item" do
			it "should add another line" do
				@multiple_choices[0].add_item(@lines[1], 1.even?)

				listItem0 = MDParse::MultipleChoiceAnswer.new(@lines[0], 0.even?)
				listItem1 = MDParse::MultipleChoiceAnswer.new(@lines[1], 1.even?)

				expect(@multiple_choices[0].content).to eq([listItem0, listItem1])
			end
		end

		describe ".to_h" do
			it "should return hash with proper type and content" do
				expect(@multiple_choices[0].to_h).to eq({type: :multipleChoice, uid: "uid", multipleSelect: true, content: @multiple_choices[0].content.map{|x| x.to_h}})
				expect(@multiple_choices[1].to_h).to eq({type: :multipleChoice, uid: "uid", multipleSelect: false, content: @multiple_choices[1].content.map{|x| x.to_h}})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::KeyFeature do
		before(:each) do
			@lines = (0..6).map{|i| "KeyFeature line " + i.to_s}
			@key_features = (0..6).map do |i|
				mc = MDParse::KeyFeature.new(i.even? ? {limit: 10, isExplicitLimit: true, shouldShowGrade: false, gradingRules: "strict"} : {})
				mc.add_item(@lines[i], i.even?, i)
				mc
			end

			@markdowns = [
# "!N Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !Y Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N Lorem ipsum dolor sit amet, consectetur adipiscing elit",

%${\"limit\": 10, \"isExplicitLimit\": true, \"shouldShowGrade\": false, \"gradingRules\": \"strict\"}
!Y1 Lorem ipsum dolor sit amet, consectetur adipiscing elit
!N1 Lorem ipsum dolor sit amet, consectetur adipiscing elit
!Y2 Lorem ipsum dolor sit amet, consectetur adipiscing elit
!N2 Lorem ipsum dolor sit amet, consectetur adipiscing elit
!P1 Lorem ipsum dolor sit amet, consectetur adipiscing elit
!P2 Lorem ipsum dolor sit amet, consectetur adipiscing elit$,

# %${"uid": "66533c30-a869-45c6-83c4-73809f6a420e"}
# !N{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N{"uid": "3be110fb-cde4-4be6-ac85-8eb03ac70796"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !Y{"uid": "5652c538-22e9-46b0-806a-641ea0281eb0"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
# !N{"uid": "ac1aca7e-4333-4016-ac4e-be3b0734a9c5"} Lorem ipsum dolor sit amet, consectetur adipiscing elit$
			]
		end

		describe ".intialize" do
			it "should set the correct original list item" do
				expect(@key_features[0].content).to eq([MDParse::KeyFeatureAnswer.new(@lines[0], 0.even?, 0)])
			end
		end

		describe ".add_line" do
			it "should add line to the last list item" do
				@key_features[0].add_line(@lines[1])

				listItem = MDParse::KeyFeatureAnswer.new(@lines[0], 0.even?, 0)
				listItem.add_line(@lines[1])

				expect(@key_features[0].content).to eq([listItem])
			end
		end

		describe ".add_item" do
			it "should add another line" do
				@key_features[0].add_item(@lines[1], 1.even?, 1)

				listItem0 = MDParse::KeyFeatureAnswer.new(@lines[0], 0.even?, 0)
				listItem1 = MDParse::KeyFeatureAnswer.new(@lines[1], 1.even?, 1)

				expect(@key_features[0].content).to eq([listItem0, listItem1])
			end
		end

		describe ".to_h" do
			it "should return hash with proper type and content" do
				expect(@key_features[0].to_h).to eq({type: :keyFeature, uid: "uid", limit: 10, isExplicitLimit: true, shouldShowGrade: false, gradingRules: "strict", content: @key_features[0].content.map{|x| x.to_h}})
				expect(@key_features[1].to_h).to eq({type: :keyFeature, uid: "uid", content: @key_features[1].content.map{|x| x.to_h}})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::SliderBar do
		before(:each) do
			@slider_bar_default = MDParse::SliderBar.new()
			@options = {minValue: 18, maxValue: 118, step: 10, showValue: true}
			@slider_bar_options = MDParse::SliderBar.new(@options)

			@markdowns = [
				%$!sliderBar{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "minValue": 10, "maxValue": 90, "step": 10, "showValue": true}$,
				%$!sliderBar$
			]
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@slider_bar_default.to_h).to eq({type: :sliderBar, uid: "uid", minValue: 0, maxValue: 100, step: 1, showValue: false, value: 0})
				expect(@slider_bar_options.to_h).to eq({type: :sliderBar, uid: "uid", minValue: 18, maxValue: 118, step: 10, showValue: true, value: 18})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::RatingBar do
		before(:each) do
			@rating_bar_default = MDParse::RatingBar.new()
			@options = {maxValue: 10}
			@rating_bar_options = MDParse::RatingBar.new(@options)

			@markdowns = [
				%$!ratingBar$,
				%$!ratingBar{"uid": "e7736398-b473-43e0-9626-e8d2651b8408", "maxValue": 13}$
			]
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@rating_bar_default.to_h).to eq({type: :ratingBar, uid: "uid", maxValue: 5, value: 0})
				expect(@rating_bar_options.to_h).to eq({type: :ratingBar, uid: "uid", maxValue: 10, value: 0})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::Checkbox do
		before(:each) do
			@lines = (0..6).map{|i| "Checkbox line " + i.to_s}
			@checkbox_items = (0..6).map{|i| MDParse::Checkbox.new(@lines[i])}

			@markdowns = [
				%$+ Lorem ipsum dolor sit amet, consectetur adipiscing elit$,
				%$+{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit$
			]
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				expect(@checkbox_items[0].to_h).to eq({type: :checkbox, uid: "uid", content: @checkbox_items[0].content, value: false})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::Matcher do
		before(:each) do
			@a_lines = (0..6).map{|i| "A line " + i.to_s}
			@b_lines = (0..6).map{|i| "B line " + i.to_s}
			@matcher = MDParse::Matcher.new()
			[[@a_lines[0], @b_lines[0]],[@a_lines[1], @b_lines[1]],[@a_lines[2], @b_lines[2]]].each{|a, b| @matcher.add_item(a,b)}

			@markdowns = [
"!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
!B Lorem ipsum dolor sit amet, consectetur adipiscing elit
!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
!B Lorem ipsum dolor sit amet, consectetur adipiscing elit
!A Lorem ipsum dolor sit amet, consectetur adipiscing elit
!B Lorem ipsum dolor sit amet, consectetur adipiscing elit",

%${"uid": "66533c30-a869-45c6-83c4-73809f6a420e"}
!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
!A{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit
!B{"uid": "e7736398-b473-43e0-9626-e8d2651b8408"} Lorem ipsum dolor sit amet, consectetur adipiscing elit$
			]
		end

		describe ".to_h" do
			it "should return a hash with correct type, content" do
				content = @matcher.content.map{|match| match.to_h}.inject([[],[]]){|tot, obj| [tot[0] << obj[0], tot[1] << obj[1]]}
				expect(@matcher.to_h).to eq({type: :matcher, uid: "uid", contentA: content[0], contentB: content[1], value: [], answer: [["uid", "uid"], ["uid", "uid"], ["uid", "uid"]]})
			end
		end

		describe ".to_md" do
			it "should return string with the proper format" do
				@markdowns.each do |markdown|
					assert_md_matches_generated_md(markdown)
				end
			end
		end
	end

	describe MDParse::FormattedText do
		describe "#parse" do
			it "escapes html" do
				html = "<h1>escape test</h1>"
				expect(MDParse::FormattedText.parse(html)).to eq(CGI::escapeHTML(html))
			end
		end
	end

	describe MDParse::Document do
		describe "#parse" do
			it "returns a section" do
				expect(MDParse::Document.parse(@texts[0])).to be_an(MDParse::Section)
			end

			it "ignors blank lines" do
				expect(MDParse::Document.parse("\n\n\t\t\t   \t\t").to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content: [], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a paragraph" do
				expect(MDParse::Document.parse(@texts[0]).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: @texts[0]}]))
			end

			it "parses an &" do
				expect(MDParse::Document.parse(@texts[0] + "&").to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: @texts[0] + "&amp;"}]))
			end

			it "parses an & with styling in line" do
				expect(MDParse::Document.parse(@texts[0] + "& *" + @texts[1] + "*").to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: @texts[0] + "&amp; <em>" + @texts[1] + "</em>"}]))
			end


			it "parses a multiline paragraph" do
				par =  @texts[0] + "   \n" + @texts[1]
				par2 = @texts[0] + " " + @texts[1]

				expect(MDParse::Document.parse(par).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: par2}]))
			end

			it "parses multiple paragraphs" do
				par = @texts[0] + "\n\n" + @texts[1]
				expect(MDParse::Document.parse(par).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: @texts[0]}, {type: :paragraph, uid: "uid", content: @texts[1]}]))
			end

			it "parses paragraphs with bracked in it" do
				par = @texts[0] + "{this is a test}" + @texts[1]
				expect(MDParse::Document.parse(par).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: par}]))
			end

			it "parses a first level section" do
				sec = "#" + @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses an n level section" do
				n = 20
				sec = "#" * n + " " * 14 + @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: n, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a multiline section" do
				section =  "#" + " " + @texts[0] + "   \n" + @texts[1]
				section_title = @texts[0] + " " + @texts[1]

				expect(MDParse::Document.parse(section).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: section_title, level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a nested section" do
				sec = "#<" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: true, collapsible: false, collapsed: false}]))
			end

			it "parses a nested collapsible collapsed section" do
				sec = "#<-" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: true, collapsible: true, collapsed: true}]))
			end

			it "parses a nested collapsible not collapsed section" do
				sec = "#<+" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], nested: true, collapsible: true, collapsed: false}]))
			end

			it "parses a section with a UID" do
				sec = "\#{\"uid\":\"test_uid\"}" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "test_uid", title: @texts[0], level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a section with classes" do
				sec = "\#{\"classes\":[\"keyword\",\"special-section\"]}" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", classes: ["keyword", "special-section"], title: @texts[0], level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a section with a inactive as true" do
				sec = "\#{\"inactive\":true}" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], inactive: 2, nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a section with a inactive as false" do
				sec = "\#{\"inactive\":false}" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], inactive: 1, nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a section with a inactive as null" do
				sec = "\#{\"inactive\":null}" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], inactive: 0, nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a section with a " do
				sec = "\#{\"invisible\":false}" + " "+ @texts[0]
				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: @texts[0], level: 1, content: [], invisible: false, nested: false, collapsible: false, collapsed: false}]))
			end

			# it "parses a section with a malformed options" do
			# 	sec_title = "{\"uid\":\"test_uid}" + " "+ @texts[0]
			# 	sec = "\#" + sec_title
			#
			# 	expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "uid", title: CGI::escapeHTML(sec_title), level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			# end

			it "parses a section with a double options hash" do
				sec_title = "{\"uid\":\"test_uid\"}" + " "+ "{\"uid\":\"test_uid\"}" + @texts[0]
				sec = "\#" + sec_title

				expect(MDParse::Document.parse(sec).to_h).to eq(root_section_wrapper([{type: :section, uid: "test_uid", title: CGI::escapeHTML("{\"uid\":\"test_uid\"}" + @texts[0]), level: 1, content: [], nested: false, collapsible: false, collapsed: false}]))
			end

			it "parses a horizontal rule" do
				image = "!horizontalRule"
				expect(MDParse::Document.parse(image).to_h).to eq(root_section_wrapper([{type: :horizontalRule, uid: "uid"}]))
			end

			it "parses a horizontal rule with info" do
				image = "!horizontalRule{\"uid\":\"test_uid\", \"classes\": [\"test_class\"]}"
				expect(MDParse::Document.parse(image).to_h).to eq(root_section_wrapper([{type: :horizontalRule, uid: "test_uid", classes: ["test_class"]}]))
			end

			it "parses an image" do
				image = "![ " + @texts[0] + "  ]( " + @texts[1] + "  )"
				expect(MDParse::Document.parse(image).to_h).to eq(root_section_wrapper([{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local}]))
			end

			it "parses an image with carrot" do
				image = "!<[ " + @texts[0] + "  ]( " + @texts[1] + "  )"
				expect(MDParse::Document.parse(image).to_h).to eq(root_section_wrapper([{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local}]))
			end

			it "parses an image without title" do
				image = "!( " + @texts[0] + "  )"
				expect(MDParse::Document.parse(image).to_h).to eq(root_section_wrapper([{type: :image, uid: "uid", title: "", source: :local, uri: @texts[0]}]))
			end

			it "parses an image with empty caption" do
				image = "![]( " + @texts[0] + "  )"
				expect(MDParse::Document.parse(image).to_h).to eq(root_section_wrapper([{type: :image, uid: "uid", title: "", source: :local, uri: @texts[0]}]))
			end

			it "parses video" do
				video = "!v[ " + @texts[0] + "  ]( " + @texts[1] + "  )"
				expect(MDParse::Document.parse(video).to_h).to eq(root_section_wrapper([{type: :video, uid: "uid", title: @texts[0], source: :local, uri: @texts[1]}]))
			end

			it "parses an image with UID" do
				uid = "12345"
				image = "![ " + @texts[0] + "  ]( " + @texts[1] + "  ){\"uid\": \"#{uid}\"}"
				expect(MDParse::Document.parse(image).to_h).to eq(root_section_wrapper([{type: :image, uid: uid, title: @texts[0], uri: @texts[1], source: :local}]))
			end

			it "parses audio" do
				audio = "!a[ " + @texts[0] + "  ]( " + @texts[1] + "  )"
				expect(MDParse::Document.parse(audio).to_h).to eq(root_section_wrapper([{type: :audio, uid: "uid", title: @texts[0], source: :local, uri: @texts[1]}]))
			end

			it "parses button" do
				button = "!b[ " + @texts[0] + "  ]"
				expect(MDParse::Document.parse(button).to_h).to eq(root_section_wrapper([{type: :button, uid: "uid", title: @texts[0], onClick: []}]))
			end

			it "parses button" do
				button = "!b[ " + @texts[0] + " ]" + "{\"onClick\": [{ \"type\" : \"action\", \"action\" : \"setNodeInactive\", \"arguments\" : {\"uid\": \"uid\", \"value\": 2}}]}"
				expect(MDParse::Document.parse(button).to_h).to eq(root_section_wrapper([{type: :button, uid: "uid", title: @texts[0], onClick: [{type: "action", action: "setNodeInactive", arguments: {uid: "uid", value: 2}}]}]))
			end

			[[:regularList, "-"], [:numberedList, "12."]].each do |list_type, list_symbol|
				it "parses a single line " + list_type.to_s do
					list = list_symbol + @texts[0]
					expect(MDParse::Document.parse(list).to_h).to eq(root_section_wrapper([{type: list_type, uid: "uid", content: [{type: :listItem, uid: "uid", content: @texts[0]}]}]))
				end

				it "parses a multi item " + list_type.to_s do
					list = list_symbol + @texts[0] + "\n" + list_symbol + "  " + @texts[1]
					expect(MDParse::Document.parse(list).to_h).to eq(root_section_wrapper([{type: list_type, uid: "uid", content: [{type: :listItem, uid: "uid", content: @texts[0]}, {type: :listItem, uid: "uid", content: @texts[1]}]}]))
				end

				it "parses a multi line " + list_type.to_s do
					list = list_symbol + @texts[0] + " \n" + @texts[1] + "  \n" + list_symbol + "  " +  @texts[2]
					expect(MDParse::Document.parse(list).to_h).to eq(root_section_wrapper([{type: list_type, uid: "uid", content: [{type: :listItem, uid: "uid", content: @texts[0] + " " + @texts[1]}, {type: :listItem, uid: "uid", content: @texts[2]}]}]))
				end
			end

			it "parses a different list types" do
				lists = "-" + @texts[0] + "\n" + "1." + "  " + @texts[1]
				expect(MDParse::Document.parse(lists).to_h).to eq(root_section_wrapper([{type: :regularList, uid: "uid", content: [{type: :listItem, uid: "uid", content: @texts[0]}]}, {type: :numberedList, uid: "uid", content: [{type: :listItem, uid: "uid", content: @texts[1]}]}]))
			end

			it "parses a document title" do
				document = "$ " + @texts[0]
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: @texts[0], level: 0, content:[], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a document title only when it is the first item" do
				document = @texts[0] + "\n\n" + "$ " + @texts[1]
				expect(MDParse::Document.parse(document).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: @texts[0]}, {type: :paragraph, uid: "uid", content: "$ " + @texts[1]}]))
			end

			it "parses a document title when it is the first non blank line" do
				document = "\n$ " + @texts[0]
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: @texts[0], level: 0, content:[], nested: false, collapsible: false, collapsed: false})
			end

			it "stored extra text" do
				document = "> " + @texts[0]
				expect(MDParse::Document.parse(document).to_h).to eq(root_section_wrapper([{type: :extra, uid: "uid", content: @texts[0]}]))
			end

			it "parses json text" do
				document = "
				!json
				{
					\"type\": \"test0\",
					\"array\": [
						\"test1\",
						\"test2\",
						\"test3\"
					],
					\"object\": {
						\"type\": \"test4\"
					}
				}
				<!json
				"
				expect(MDParse::Document.parse(document).to_h).to eq(root_section_wrapper([{"type"=>"test0", "array"=>["test1", "test2", "test3"], "object"=>{"type"=>"test4"}}]))
			end

			it "parses malformed json text" do
				document = "
				!json
				{
					\"type\": \"test0\",
					\"array\": [
						\"test1\",
						\"test2\",
						\"test3\"
					],
					\"object\":
						\"type\": \"test4\"
					}
				}
				<!json
				"
				expect(MDParse::Document.parse(document).to_h).to eq(root_section_wrapper([{type: :extra, uid: "uid", subtype: :error, content: "Malformed JSON"}]))
			end

			it "parses a regular text input" do
				document = "!textInput"
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::TextInput.new().to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a regular text input with ti" do
				document = "!ti"
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::TextInput.new().to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a text input with options" do
				document = '!textInput{"characterLimit": 1000, "boxLines": 10}'
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::TextInput.new(characterLimit: 1000, boxLines: 10).to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a multiple choice" do
				document = "!N Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!N Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!N Lorem ipsum dolor sit amet, consectetur adipiscing elit"

				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						{type: :multipleChoice, uid: "uid", multipleSelect: false, content: [
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false},
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false},
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: true},
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false}
						]}
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a multiple choice with multiple select" do
				document = "{\"multipleSelect\": true}\n!N Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!N Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!Y Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!N Lorem ipsum dolor sit amet, consectetur adipiscing elit"

				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						{type: :multipleChoice, uid: "uid", multipleSelect: true, content: [
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false},
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false},
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: true},
							{type: :multipleChoiceAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false}
						]}
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a key feature" do
				document = "!N1 Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!Y2 Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!Y1 Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!P1 Lorem ipsum dolor sit amet, consectetur adipiscing elit"

				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						{type: :keyFeature, uid: "uid", content: [
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false, keyFeature: 1},
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: true, keyFeature: 2},
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: true, keyFeature: 1},
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: 'poison', keyFeature: 1}
						]}
					], nested: false, collapsible: false, collapsed: false})
			end


			it "parses a key feature with options" do
				document = "{\"limit\": 10, \"isExplicitLimit\": true, \"shouldShowGrade\": false, \"gradingRules\": \"strict\"}\n!N1 Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!Y2 Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!Y1 Lorem ipsum dolor sit amet, consectetur adipiscing elit\n!P1 Lorem ipsum dolor sit amet, consectetur adipiscing elit"

				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						{type: :keyFeature, uid: "uid", limit: 10, isExplicitLimit: true, shouldShowGrade: false, gradingRules: 'strict', content: [
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: false, keyFeature: 1},
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: true, keyFeature: 2},
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: true, keyFeature: 1},
							{type: :keyFeatureAnswer, uid: "uid", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", value: false, answer: 'poison', keyFeature: 1}
						]}
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a slider bar" do
				document = '!sliderBar'
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::SliderBar.new().to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a slider bar with options" do
				document = '!sliderBar{"minValue": 10, "maxValue": 100, "step": 5}'
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::SliderBar.new(minValue: 10, maxValue: 100, step: 5).to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a rating bar" do
				document = '!ratingBar'
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::RatingBar.new().to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a rating bar with options" do
				document = '!ratingBar{"maxValue": 10}'
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::RatingBar.new(maxValue: 10).to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a checkbox" do
				document = '+ ' + @texts[0]
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						MDParse::Checkbox.new(@texts[0]).to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "parses a matcher" do
				document = "!A " + @texts[0] + "\n!B " + @texts[1] + "\n!A " + @texts[2] + "\n!B " + @texts[3]
				matcher = MDParse::Matcher.new()
				@texts[0..3].each_slice(2).to_a.each{|a, b| matcher.add_item(a, b)}
				expect(MDParse::Document.parse(document).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
						matcher.to_h
					], nested: false, collapsible: false, collapsed: false})
			end

			it "nests non-sections under sections" do
				section =  "#" + " " + @texts[0] + "   \n" + @texts[1]
				par =  @texts[2] + "   \n" + @texts[3]
				image = "![ " + @texts[4] + "  ]( " + @texts[5] + "  )"

				text = section + "\n\n" + par + "\n\n" + image + "\n" + section + "\n\n" + image

				expect(MDParse::Document.parse(text).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
														{type: :section, uid: "uid", title: @texts[0] + " " + @texts[1], level: 1,
															content: [{type: :paragraph, uid: "uid", content: @texts[2] + " " + @texts[3]},
																				{type: :image, uid: "uid", title: @texts[4], source: :local, uri: @texts[5]}],
															nested: false, collapsible: false, collapsed: false},
													  {type: :section, uid: "uid", title: @texts[0] + " " + @texts[1], level: 1,
															content: [{type: :image, uid: "uid", title: @texts[4], source: :local, uri: @texts[5]}],
															nested: false, collapsible: false, collapsed: false}
														], nested: false, collapsible: false, collapsed: false});

			end

			it "properly nests sections" do
				section1 = "#" + @texts[0] + "\n\n"
				section2 = "##" + @texts[1] + "\n\n"
				section3 = "###" + @texts[2] + "\n\n"

				par =  @texts[3] + "\n\n"

				text = section1 + section2 + section2 + section1 + par + section3 + par + section2 + section3 + section1

				expect(MDParse::Document.parse(text).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
															], nested: false, collapsible: false, collapsed: false},
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
															], nested: false, collapsible: false, collapsed: false}
														], nested: false, collapsible: false, collapsed: false},
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
															{type: :paragraph, uid: "uid", content: @texts[3]},
															{type: :section, uid: "uid", title: @texts[2], level: 3, content:[
																{type: :paragraph, uid: "uid", content: @texts[3]}
															], nested: false, collapsible: false, collapsed: false},
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
																{type: :section, uid: "uid", title: @texts[2], level: 3, content:[
																], nested: false, collapsible: false, collapsed: false}
															], nested: false, collapsible: false, collapsed: false}
														], nested: false, collapsible: false, collapsed: false},
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
														], nested: false, collapsible: false, collapsed: false}
													], nested: false, collapsible: false, collapsed: false})

			end

			it "properly nests nested sections" do
				section1 = "#" + @texts[0] + "\n\n"
				section2 = "##<" + @texts[1] + "\n\n"
				section3 = "###" + @texts[2] + "\n\n"

				par =  @texts[3] + "\n\n"

				text = section1 + section2 + section2 + section1 + par + section3 + par + section2 + section3 + section1

				expect(MDParse::Document.parse(text).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
															], nested: true, collapsible: false, collapsed: false},
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
															], nested: true, collapsible: false, collapsed: false}
														], nested: false, collapsible: false, collapsed: false},
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
															{type: :paragraph, uid: "uid", content: @texts[3]},
															{type: :section, uid: "uid", title: @texts[2], level: 3, content:[
																{type: :paragraph, uid: "uid", content: @texts[3]}
															], nested: false, collapsible: false, collapsed: false},
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
																{type: :section, uid: "uid", title: @texts[2], level: 3, content:[
																], nested: false, collapsible: false, collapsed: false}
															], nested: true, collapsible: false, collapsed: false}
														], nested: false, collapsible: false, collapsed: false},
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
														], nested: false, collapsible: false, collapsed: false}
													], nested: false, collapsible: false, collapsed: false})

			end

			it "properly ends nested sections with <## tag" do
				section1 = "#" + @texts[0] + "\n\n"
				end_section1_tag = "<#\n\n"
				section2 = "##" + @texts[1] + "\n\n"
				end_section2_tag = "<##\n\n"
				section3 = "###" + @texts[2] + "\n\n"

				par =  @texts[3] + "\n\n"

				text = section1 + section2 + end_section2_tag + par + section1 + par + section3 + par + section2 + section3 + end_section1_tag + par + section1

				expect(MDParse::Document.parse(text).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
															], nested: false, collapsible: false, collapsed: false},
															{type: :paragraph, uid: "uid", content: @texts[3]}
														], nested: false, collapsible: false, collapsed: false},
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
															{type: :paragraph, uid: "uid", content: @texts[3]},
															{type: :section, uid: "uid", title: @texts[2], level: 3, content:[
																{type: :paragraph, uid: "uid", content: @texts[3]}
															], nested: false, collapsible: false, collapsed: false},
															{type: :section, uid: "uid", title: @texts[1], level: 2, content:[
																{type: :section, uid: "uid", title: @texts[2], level: 3, content:[
																], nested: false, collapsible: false, collapsed: false}
															], nested: false, collapsible: false, collapsed: false}
														], nested: false, collapsible: false, collapsed: false},
														{type: :paragraph, uid: "uid", content: @texts[3]},
														{type: :section, uid: "uid", title: @texts[0], level: 1, content:[
														], nested: false, collapsible: false, collapsed: false}
													], nested: false, collapsible: false, collapsed: false})

			end

			it "parses a multiline line" do
				title = "$" + @texts[0] + "\n" + @texts[1]

				expect(MDParse::Document.parse(title).to_h).to eq({type: :section, uid: "uid", title: @texts[0] + " " + @texts[1], level: 0, content:[], nested: false, collapsible: false, collapsed: false})
			end

			it "properly parse bolded link" do
				text = "**l[Google](www.google.com) ipsum**"
				text2 = "**[Google](www.google.com) ipsum**"
				expect(MDParse::Document.parse(text).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: "<strong>l<a href=\"http://www.google.com\" target=\"_blank\">Google</a> ipsum</strong>"}]))
				expect(MDParse::Document.parse(text2).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: "<strong><a href=\"http://www.google.com\" target=\"_blank\">Google</a> ipsum</strong>"}]))
			end

			it "properly parses apostrophe link" do
				text = "l[Google's](www.google.com) ipsum"
				expect(MDParse::Document.parse(text).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: "l<a href=\"http://www.google.com\" target=\"_blank\">Google&#39;s</a> ipsum"}]))
			end

			it "properly parses apostrophe link" do
				text = "l^this's^ ipsum"
				expect(MDParse::Document.parse(text).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: "l<sup>this&#39;s</sup> ipsum"}]))
			end


			it "properly parse style in link" do
				text = "[*Apple*](www.apple.com)"

				expect(MDParse::Document.parse(text).to_h).to eq(root_section_wrapper([{type: :paragraph, uid: "uid", content: "<a href=\"http://www.apple.com\" target=\"_blank\"><em>Apple</em></a>"}]))
			end

			it "properly parses images into galleries and individual images" do
				image = "![ " + @texts[0] + "  ]( " + @texts[1] + "  )\n"

				text = image + "\n" + image + image + image + "\n" + image + image + "\n" + image

				expect(MDParse::Document.parse(text).to_h).to eq({type: :section, uid: "uid", title: "", level: 0, content:[
												{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local},
												{type: :imageGallery, uid: "uid", images:  [{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local},{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local},{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local}]},
												{type: :imageGallery, uid: "uid", images:  [{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local},{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local}]},
												{type: :image, uid: "uid", title: @texts[0], uri: @texts[1], source: :local}
											], nested: false, collapsible: false, collapsed: false})
			end
		end
	end
end

describe "MDParse2" do
	it "properly parses and generates md for test file" do
		assert_md_matches_generated_md(File.read("spec/test.md"))
	end
end
