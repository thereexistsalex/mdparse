module MDParse
	class Media < UIDObject
		include TitleSection
		attr_reader :uri, :source

		MEDIA_MD_MARK = "m"

		def initialize(title, uri, options={})
			super(options)
			@original_title = ""
			add_line(title)

			match = uri_filters.map{|uri_filter| [uri.match(uri_filter[0]), uri_filter[1]] }.reject{|match| match[0].nil?}.first
			if match
				@uri = match[0][:uri]
				@source = match[1]
			else
				@uri = uri
				@source = :local
			end
		end

		def uri_filters
			[]
		end

		def to_h
			super.merge({title: self.title, source: @source, uri: @uri})
		end

		def to_md
			"!" + self.class::MEDIA_MD_MARK + "[" + self.original_title + "]" + "(" + ((@source != :local) ? self.uri_link_prefixes[self.source] : "") + self.uri + ")" + super
		end
	end
end
