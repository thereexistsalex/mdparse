module MDParse
	class Button < UIDObject
		include TitleSection

		def initialize(title, options={})
			super(options)
			@original_title = ""
			add_line(title)
		end

		def to_h
			{onClick: []}.merge(super).merge({title: self.title})
		end

		def to_md
			"!b[" + self.original_title + "]" + super
		end
	end
end
