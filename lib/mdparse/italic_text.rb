module MDParse
	class ItalicText < Paragraph
		def to_h
			"<em>" + self.content + "</em>"
		end

		def mdparse_original_text
			self.content = MDParse::FormattedText.already_escaped_parse(@original_text)
		end
	end
end
