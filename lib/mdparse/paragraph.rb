module MDParse
	class Paragraph < UIDObject
		attr_reader :original_text
		attr_writer :content

		def content
			@content = @content || []
		end

		def to_h
			super.merge({content: self.content})
		end

		def to_md
			super + " " + @original_text
		end

		def <<(o)
			self.content << o
		end

		def initialize(line, options={})
			super(options)
			@original_text = ""
			add_line(line)
		end

		def add_line(line)
			if @original_text.empty?
			elsif @original_text[-1] == "-"
				@original_text = @original_text[0...-1]
			else
				@original_text << " "
			end

			@original_text << line.strip
			mdparse_original_text()
		end

		def ==(o)
			(o.class == self.class) && (self.content == o.content) && (self.original_text == o.original_text)
		end

		def to_s
			self.content.join
		end

		private

		def mdparse_original_text
			self.content = MDParse::FormattedText.parse(@original_text)
		end
	end
end
