module MDParse
	class MatchItem < Paragraph
		def to_md(i=0)
			"!" + (i==0 ? "A" : "B") + super()
		end
	end
end
