module MDParse
	class SubscriptText < Paragraph
		def to_h
			"<sub>" + self.content + "</sub>"
		end
	end
end
