module MDParse
  class UIDObject
    attr_reader :options

    DEFAULTS = {}

    def initialize(options = {})
      @options = all_defaults.merge(options)
      @options[:uid] = SecureRandom.uuid unless @options[:uid]

      @options = Hash[@options.map{ |k, o| [k, o.is_a?(Proc) ? o.call(@options) : o ] }]
    end

    def all_defaults
      ancestors = self.class.ancestors
      ancestors_till_UIDObject = ancestors[0..ancestors.index(UIDObject)]
      ancestors_till_UIDObject.map{|c| c::DEFAULTS}.inject{|total, one| one.merge(total)}
    end

    def to_h
      h = {type: self.uncapitalized_name.to_sym}.merge(@options)

      if h.has_key?(:inactive)
        h[:inactive] = {false => 1, true => 2}[h[:inactive]]
        h[:inactive] = 0 if h[:inactive].nil?
      end

      h
    end

    def to_md
      JSON.generate(@options.reject{|k,v| [:answer, :collapsed, :collapsible, :content, :contentA, :contentB, :images, :keyFeature, :level, :nested, :source, :subtype, :title, :type, :uri, :value].include?(k)})
    end

    def uncapitalized_name
      self.class.to_s.gsub(/^.*::/, '').uncapitalize
    end
  end
end
