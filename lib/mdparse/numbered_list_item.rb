module MDParse
	class NumberedListItem < Paragraph
		def to_md(i=0)
			"#{i+1}." + super()
		end

    def to_h
      super.merge({type: :listItem})
    end
	end
end
