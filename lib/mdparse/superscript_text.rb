module MDParse
	class SuperscriptText < Paragraph
		def to_h
			"<sup>" + self.content + "</sup>"
		end
	end
end
