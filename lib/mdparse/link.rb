module MDParse
	class Link < Media
		MEDIA_MD_MARK = ""

		def url
			if self.uri =~ /(:\/\/|mailto:)/
				self.uri
			elsif self.uri =~ /(:\/\/|tel:)/
				self.uri
			elsif self.uri =~ /^\//
				self.uri
			else
				"http://" + self.uri
			end
		end

		def to_h
			"<a href=\"#{self.url}\" target=\"_blank\">#{self.title}</a>"
		end
	end
end
