module MDParse
  class ParseError
    def initialize(original_md, line)
      @original_md = original_md
      @line = line
    end

    def to_md
      @original_md
    end

    def to_h
      MDParse::Extra.new("Malformed Options Hash In Line...\n#{@line}").to_h.merge({subtype: :error})
    end
  end
end
