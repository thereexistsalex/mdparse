module MDParse
 class KeyFeatureAnswer < Paragraph
   DEFAULTS = {
     value: false
   }

   def initialize(line, answer, key_feature, options={})
     super(line, options)
     @answer = answer
     @key_feature = key_feature
   end

   def to_h
     super.merge({answer: @answer, keyFeature: @key_feature})
   end

   def to_md(i)
     "!" + (@answer == 'poison' ? 'P' : (@answer ? "Y" : "N")) + @key_feature.to_s + super()
   end
 end
end
