module MDParse
	class Section < ContentSection
		include TitleSection

		attr_accessor :level
		attr_accessor :nested
		attr_accessor :parent
		attr_accessor :collapsible
		attr_accessor :collapsed

		def initialize(title, level, options={})
			super(options)
			@original_title = ""
			add_line(title)
			@level = level
			@nested = false
			@parent = nil
			@collapsible = false
			@collapsed = false
		end

		def <<(o)
			o.parent = self if o.class == Section
			super
		end

		def to_h
			h = super.merge({title: self.title, level: self.level, collapsible: @collapsible, collapsed: @collapsed})

			if self.nested
				h.merge({nested: true})
			else
				h.merge({nested: false})
			end
		end

		def to_md
			(self.level == 0 ? "$" : "#" * self.level) +
			(@nested ? "<" + (@collapsible ? (@collapsed ? "-" : "+") : "") : "") +
			super + " " +
			self.original_title + "\n\n" +
			self.content.each_with_index.map do |x, i|
				next_x = self.content[i+1]

				extra = if x.class == Section && next_x && (next_x.class != Section || (next_x.level > x.level))
					"\n\n" + "<" + "#" * x.level
				else
					""
				end
				x.to_md + extra
			end.join("\n\n")
		end

		def self.regex
			/\A(#+)(.*)/
		end

		def ==(o)
			super(o) && (self.title == o.title) && (self.level == o.level)
		end

		def child_sections
			self.content.select{|x| x.class == Section}
		end

		def decendent_sections
			self.child_sections.map{|x| [x] + x.decendent_sections }.flatten
		end

		def crypto_hash_string
			(Digest::SHA2.new << self.to_h.to_s).to_s
		end
	end
end
