module MDParse
	class FormattedText
		def self.already_escaped_parse(ex_text)
			formatted_match_data = /(^|[^\*\\])(\*+)([^\*].*?[^\*\\])(\2)($|[^\*])/.match(ex_text)
			superscript_match_data = /(^|[^\\])(\^)(((\\\^)|[^\^])*)(\^)/.match(ex_text)
			subscript_match_data = /(^|[^\\])(\~)(((\\\~)|[^\~])*)(\~)/.match(ex_text)
			link_match_data = /(^|[^\!])(\[)([^\[\]]+)\]\(([^\(\)]+)\)/.match(ex_text)

			first_match = [formatted_match_data, superscript_match_data, subscript_match_data, link_match_data].compact.map{|match_data| match_data.begin(2)}.min

			if formatted_match_data && (formatted_match_data.begin(2) == first_match)
				formatted_text = case formatted_match_data[2].length
					when 1
						MDParse::ItalicText.new(ex_text[formatted_match_data[3]]).to_h
					when 2
						MDParse::BoldText.new(ex_text[formatted_match_data[3]]).to_h
					when 3
						MDParse::UnderlinedText.new(ex_text[formatted_match_data[3]]).to_h
					else
						puts "Style marking: #{formatted_match_data[2]} too long"
				end

				return  MDParse::FormattedText.already_escaped_parse(ex_text[0...formatted_match_data.end(1)]) +
		        		formatted_text +
		        		MDParse::FormattedText.already_escaped_parse(ex_text[formatted_match_data.begin(5)..-1])
	    elsif link_match_data && (link_match_data.begin(2) == first_match)
    		return  MDParse::FormattedText.already_escaped_parse(ex_text[0...link_match_data.begin(0)] + link_match_data[1]) +
								MDParse::Link.new(CGI::unescapeHTML(link_match_data[3]), link_match_data[4]).to_h +
								MDParse::FormattedText.already_escaped_parse(ex_text[link_match_data.end(0)..-1])
			elsif superscript_match_data && (superscript_match_data.begin(2) == first_match)
				return  MDParse::FormattedText.already_escaped_parse(ex_text[0...superscript_match_data.end(1)]) +
		        		MDParse::SuperscriptText.new(CGI::unescapeHTML(ex_text[superscript_match_data[3]])).to_h +
		        		MDParse::FormattedText.already_escaped_parse(ex_text[superscript_match_data.end(0)..-1])
			elsif subscript_match_data && (subscript_match_data.begin(2) == first_match)
				return  MDParse::FormattedText.already_escaped_parse(ex_text[0...subscript_match_data.end(1)]) +
		        		MDParse::SubscriptText.new(CGI::unescapeHTML(ex_text[subscript_match_data[3]])).to_h +
		        		MDParse::FormattedText.already_escaped_parse(ex_text[subscript_match_data.end(0)..-1])
			else
				case ex_text
				when ""
					return ""
				else
					return ex_text
				end
			end
		end

		def self.parse(text)
			self.already_escaped_parse(CGI::escapeHTML(text))
		end
	end
end
