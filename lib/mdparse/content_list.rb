module MDParse
	class ContentList < ContentSection
    ITEM_CLASS = MDParse::ListItem
    def add_line(line)
      self.content.last.add_line(line)
    end

    def add_item(*line)
      self << self.class::ITEM_CLASS.new(*line)
    end

    def to_md
      super + "\n" + self.content.each_with_index.map{|x,i| x.to_md(i)}.join("\n")
    end
  end
end
