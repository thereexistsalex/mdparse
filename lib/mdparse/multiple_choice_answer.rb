module MDParse
 class MultipleChoiceAnswer < Paragraph
   DEFAULTS = {
     value: false
   }

   def initialize(line, answer, options={})
     super(line, options)
     @answer = answer
   end

   def to_h
     super.merge({answer: @answer})
   end

   def to_md(i)
     "!" + (@answer ? "Y" : "N") + super()
   end
 end
end
