module MDParse
	class Video < Media
		MEDIA_MD_MARK = "v"

    def uri_filters
      [
        [/youtu.be\/(?<uri>[\w-]+)/, :youtube],
				[/youtube.com\/embed\/(?<uri>[\w-]+)/, :youtube],
				[/vimeo.com\/(.+\/)?(?<uri>\d+)/, :vimeo],
				[/reeldx.com\/(.+\/)?(?<uri>[\w\.]+)/, :reeldx]
      ]
    end

		def uri_link_prefixes
			{
				youtube: "youtu.be/",
				vimeo: "vimeo.com/",
				reeldx: "reeldx.com/"
			}
		end
	end
end
