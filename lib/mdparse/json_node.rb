module MDParse
	class JSONNode
		def initialize(json)
      @json = json
		end

		def to_md
			"!json\n" + @json + "<!json"
		end

		def to_h
      begin
        JSON.parse(@json)
      rescue
				MDParse::Extra.new("Malformed JSON").to_h.merge({subtype: :error})
      end
		end
	end
end
