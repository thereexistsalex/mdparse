module MDParse
	module TitleSection
		attr_reader :original_title, :title

		DEFAULTS = {}

		def add_line(line)
			@original_title << " " unless @original_title == ""
			@original_title << line.strip
			mdparse_original_title
		end

		private

		def mdparse_original_title
			@title = MDParse::FormattedText.parse(@original_title)
		end
	end
end
