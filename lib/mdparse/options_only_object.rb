module MDParse
  class OptionsOnlyObject < UIDObject
    DEFAULTS = {}

    def to_md
      "!" + self.uncapitalized_name + JSON.generate(@options)
    end
  end
end
