module MDParse
	class ContentSection < UIDObject
		attr_writer :content

		def content
			@content = @content || []
		end

		def to_h
			super.merge({content: self.content.map{|x| x.to_h}})
		end

		def <<(o)
			self.content << o
		end

		def ==(o)
			(o.class == self.class) && (self.content == o.content)
		end

		# def content_map!(&block)
		# 	self.content = self.content.map do |c|
		# 		if c.class <= ContentSection
		# 			c.content_map(&block)
		# 		else
		# 			yield c
		# 		end
		# 	end
		#
		# 	yield self
		# end
		#
		# def content_map(&block)
		# 	self.dup.content_map!(&block)
		# end
		#
		# def content_inject(acc, &block)
		# 	child_acc = self.content.inject(acc) do |acc_in, c|
		# 		if c.class <= ContentSection
		# 			c.content_inject(acc_in, &block)
		# 		else
		# 			yield(acc_in, c)
		# 		end
		# 	end
		#
		# 	yield(child_acc, self)
		# end
		#
		# def content_flatten
		# 	self.content_inject([]) do |acc, c|
		# 		[c] + acc
		# 	end
		# end
		#
		# def content_select(&block)
		# 	self.content_map! do |node|
		# 		if node.class <= ContentSection
		# 			node.content = node.content.select(&block)
		# 			node
		# 		else
		# 			node
		# 		end
		# 	end
		# end
		#
		# def content_each(&block)
		# 	yield self
		#
		# 	content.each do |c|
		# 		if c.class <= ContentSection
		# 			c.content_each(&block)
		# 		end
		# 	end
		# end
		#
		# def content_flatten_select(&block)
		# 	self.content_flatten.select(&block)
		# end
	end
end
