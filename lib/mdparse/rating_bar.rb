module MDParse
  class RatingBar < OptionsOnlyObject
    DEFAULTS = {
      maxValue: 5,
      value: 0
    }
  end
end
