# BASE_OPTIONS_CAPTURE=/(?:
#   (?<number>-?(?=[1-9]|0(?!\d))\d+(?:\.\d+)?(?:[eE][+-]?\d+)?)
#   (?<boolean>true|false|null)
#   (?<string>"(?:[^"\\]++|\\["\\bfnrt\/]|\\u[0-9a-f]{4})*")
#   (?<array>\[(?>\g<json>(?:,\g<json>)*)?\s*\])
#   (?<pair>\s*\g<string>\s*:\g<json>)
#   (?<object>\{(?>\g<pair>(?:,\g<pair>)*)?\s*\})
#   (?<json>\s*(?>\g<number>|\g<boolean>|\g<string>|\g<array>|\g<object>)\s*)
# ){0}\g<object>/

BASE_OPTIONS_CAPTURE=/(?:
  (?<object>\{([^\{\}]|\g<object>)*\})
){0}\g<object>/

FORCE_OPTIONS_CAPTURE=/(?<options_json>#{BASE_OPTIONS_CAPTURE})/

OPTIONS_CAPTURE=/(?<options_json>#{BASE_OPTIONS_CAPTURE})?/

REST_CAPTURE=/(?<rest>.*)/

BRACKET_TITLE_CAPTURE=/(\[\s*(?<bracket_title>[^\[\]]*?)\s*\])?/

class MatchData
	def doc_options
		self[:options_json] ? JSON.parse(self[:options_json], symbolize_names: true).keys_to_sym : {}
	end

	def doc_rest
		self[:rest].strip
	end

  def doc_bracket_title
    self[:bracket_title] ? self[:bracket_title].strip : ""
  end
end

module MDParse
	class Document
		def self.parse_file(input_file, output_folder)
			input_text = File.read(input_file)

			FileUtils::mkdir_p(output_folder)

			self.parse_text_and_yield(input_text) do |json, file_name|
				file_path = File.join(output_folder, file_name)
				File.open(file_path,"w") {|f| f.write(json)}
			end
		end

		def self.parse_text_and_yield(text, &block)
			parsed_hash = Document.parse(text).to_h
			yield JSON.pretty_generate(parsed_hash), "Contents.json"
		end

		def self.parse(text)
			begin
				current_object = nil
				current_section_stack = [Section.new("", 0)]
				lines = text.lines

				lines.shift while lines.first && lines.first.strip.empty?

				# Document Title
				line = lines.first
				if /\A\$#{OPTIONS_CAPTURE}#{REST_CAPTURE}/.match(lines.first)
          current_section_stack = [Section.new($~.doc_rest, 0, $~.doc_options)]
          current_object = current_section_stack[0]

					lines.shift
				end

				new_options = {}

				while !lines.empty?
					line = lines.shift

					old_options = new_options
					new_options = {}

					case line
					# Sections
					when /\A(?<section_hashes>#+)(?<section_nested><?)(?<section_collapsible>[+-]?)#{OPTIONS_CAPTURE}#{REST_CAPTURE}/
            section_level = $~[:section_hashes].length
            section_nested = !$~[:section_nested].empty?
            section_collapsible = !$~[:section_collapsible].empty?
            section_collapsed = $~[:section_collapsible] == '-'

            new_section = Section.new($~.doc_rest, section_level, $~.doc_options)
            new_section.nested = section_nested
            new_section.collapsible = section_collapsible
            new_section.collapsed = section_collapsed

						until current_section_stack[-1].level < new_section.level
							current_section_stack.pop
						end

						current_section_stack[-1] << new_section
						current_section_stack << new_section
						current_object = new_section
					# Images/Video/Audio
          when /\A\!(?<media_type>[vai]?)(?<media_embed><?)#{BRACKET_TITLE_CAPTURE}\(\s*(?<media_uri>[^\(\)]+?)\s*\)#{OPTIONS_CAPTURE}/
						uri = $~[:media_uri].strip
						embed = !$~[:media_embed].empty?
						type = $~[:media_type]

						case type
						when "v"
							current_object = Video.new($~.doc_bracket_title, uri, $~.doc_options)
							current_section_stack[-1] << current_object
						when "a"
							current_object = Audio.new($~.doc_bracket_title, uri, $~.doc_options)
							current_section_stack[-1] << current_object
						else
							unless current_object && current_object.class == ImageGallery
								current_object = ImageGallery.new(Image.new($~.doc_bracket_title, uri, $~.doc_options), old_options)
								current_section_stack[-1] << current_object
							else
								current_object.add_image(Image.new($~.doc_bracket_title, uri, $~.doc_options))
							end
						end
					when /\A\!b#{BRACKET_TITLE_CAPTURE}#{OPTIONS_CAPTURE}/
						current_object = Button.new($~.doc_bracket_title, $~.doc_options)
						current_section_stack[-1] << current_object
					when /\A\!(ti|textInput)#{OPTIONS_CAPTURE}/
            current_object = TextInput.new($~.doc_options)
						current_section_stack[-1] << current_object
					when /\A\!(sliderBar)#{OPTIONS_CAPTURE}/
            current_object = SliderBar.new($~.doc_options)
						current_section_stack[-1] << current_object
					when /\A\!(ratingBar)#{OPTIONS_CAPTURE}/
            current_object = RatingBar.new($~.doc_options)
						current_section_stack[-1] << current_object
          when /\A\!(hr|horizontalRule)#{OPTIONS_CAPTURE}/
            current_object = HorizontalRule.new($~.doc_options)
            current_section_stack[-1] << current_object
					when /\A\+#{OPTIONS_CAPTURE}#{REST_CAPTURE}/
            current_object = Checkbox.new($~.doc_rest, $~.doc_options)
            current_section_stack[-1] << current_object
					when /\A!A#{OPTIONS_CAPTURE}#{REST_CAPTURE}/
            options_a = $~.doc_options
            matcher_content_a = $~.doc_rest

						second_line_b_match = lines[0] && lines[0].match(/\A!B#{OPTIONS_CAPTURE}(.*)/)
            if lines[0] && lines[0].match(/\A!B#{OPTIONS_CAPTURE}#{REST_CAPTURE}/)
              lines.shift
              options_b = $~.doc_options
              matcher_content_b = $~.doc_rest

							unless current_object && current_object.class == Matcher
								current_object = Matcher.new(old_options)
								current_section_stack[-1] << current_object
							end

							current_object.add_item(matcher_content_a, matcher_content_b, options_a, options_b)
						else
							current_object = Paragraph.new(line.strip)
							current_section_stack[-1] << current_object
						end
					when /\A(?<list_symbol>\+|\-|\d+\.)#{OPTIONS_CAPTURE}#{REST_CAPTURE}/
            options = $~.doc_options
            rest = $~.doc_rest

						list_class = case $~[:list_symbol]
						when /\-/
							RegularList
						when /\+/
							Checklist
						else
							NumberedList
						end

						unless current_object && current_object.class == list_class
							current_object = list_class.new(old_options)
							current_section_stack[-1] << current_object
						end
						current_object.add_item(rest, options)
          when /\A\!(?<choice_answer>N|Y|P)(?<choice_group>\d+)?#{OPTIONS_CAPTURE}#{REST_CAPTURE}/
            if $~[:choice_group]
              choice_answer = case $~[:choice_answer]
              when "Y"
                true
              when "N"
                false
              when "P"
                'poison'
              else
                false
              end

              choice_group = $~[:choice_group].to_i

              unless current_object && current_object.class == KeyFeature
                current_object = KeyFeature.new(old_options)
                current_section_stack[-1] << current_object
              end
              current_object.add_item($~.doc_rest, choice_answer, choice_group, $~.doc_options)
            else
              choice_answer = $~[:choice_answer] == "Y"

  						unless current_object && current_object.class == MultipleChoice
  							current_object = MultipleChoice.new(old_options)
  							current_section_stack[-1] << current_object
  						end
              current_object.add_item($~.doc_rest, choice_answer, $~.doc_options)
            end
					# JSON
					when /\A\s*\!json/
						json = ""

						while !lines.empty?
							line = lines.shift

							if line.match(/\A\s*<\!json/)
								break
							else
								json = json + line
							end
						end

						current_section_stack[-1] << JSONNode.new(json)
					# End Section
					when /\A<(#+)/
						section_level = $1.length

						until current_section_stack[-1].level < section_level
							current_section_stack.pop
						end
					# Extra Text
					when /\A>#{OPTIONS_CAPTURE}#{REST_CAPTURE}/
            unless current_object && current_object.class == Extra
              current_object = Extra.new($~.doc_rest, $~.doc_options)
              current_section_stack[-1] << current_object
            else
              current_object.add_line($~.doc_rest)
            end
					when /\A#{FORCE_OPTIONS_CAPTURE}\s*\z/
            new_options = $~.doc_options
					when /\A#{FORCE_OPTIONS_CAPTURE}#{REST_CAPTURE}/
            current_object = Paragraph.new($~.doc_rest, $~.doc_options)
            current_section_stack[-1] << current_object
					when /\A\S/
						unless current_object && [Paragraph, Section, Extra, RegularList, Checklist, NumberedList].include?(current_object.class)
							current_object = Paragraph.new(line.strip, old_options)
							current_section_stack[-1] << current_object
						else
							current_object.add_line(line.strip)
						end
					else
						current_object = nil
					end
				end
				return current_section_stack[0]
			rescue JSON::ParserError
				return ParseError.new(text, line)
			end
		end
	end
end
