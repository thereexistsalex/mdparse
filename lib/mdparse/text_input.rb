module MDParse
  class TextInput < OptionsOnlyObject
    DEFAULTS = {
      boxLines: 1,
      value: ""
    }
  end
end
