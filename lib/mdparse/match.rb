module MDParse
	class Match < Paragraph
    attr_reader :a, :b

    def initialize(a, b, options_a={}, options_b={})
      @a = MDParse::MatchItem.new(a, options_a)
      @b = MDParse::MatchItem.new(b, options_b)
    end

		def to_md(i)
      @a.to_md(0) + "\n" + @b.to_md(1)
		end

    def to_h
      [@a.to_h, @b.to_h]
    end
	end
end
