module MDParse
  class MultipleChoice < ContentList
    ITEM_CLASS = MDParse::MultipleChoiceAnswer
    DEFAULTS = {
      multipleSelect: false
    }

    def to_md
      JSON.generate(@options) + "\n" + self.content.each_with_index.map{|x,i| x.to_md(i)}.join("\n")
    end
  end
end
