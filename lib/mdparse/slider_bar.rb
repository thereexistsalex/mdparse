module MDParse
  class SliderBar < OptionsOnlyObject
    DEFAULTS = {
      minValue: 0,
      maxValue: 100,
      step: 1,
      showValue: false,
      value: lambda{|options| options[:minValue]}
    }
  end
end
