module MDParse
	class BoldText < Paragraph
		def to_h
			"<strong>" + self.content + "</strong>"
		end

		def mdparse_original_text
			self.content = MDParse::FormattedText.already_escaped_parse(@original_text)
		end
	end
end
