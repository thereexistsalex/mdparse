class String
	def to_h
		return self
	end

	def json_name
		self.split.map{|word| word.capitalize}.join.gsub(/\W/, "")[0..20] + ".json"
	end

	def json_name_with_string(string)
		self.split.map{|word| word.capitalize}.join.gsub(/\W/, "")[0..20] + "-" + string[0..20] + ".json"
	end

  def uncapitalize
    self[0, 1].downcase + self[1..-1]
  end
end
