module MDParse
	class Audio < Media
		MEDIA_MD_MARK = "a"

    def uri_filters
      [
        [/api.soundcloud.com\/tracks\/(?<uri>\w+)/, :soundcloud],
      ]
    end

		def uri_link_prefixes
			{
				soundcloud: "api.soundcloud.com/tracks/"
			}
		end
	end
end
