module MDParse
	class ImageGallery < UIDObject
		attr_reader :images

		def initialize(image = nil, options={})
			super(options)
			@images = image ? [image] : []
		end

    def add_image(image)
      @images << image
    end

		def to_h
      if @images.count == 1
        @images.first.to_h
      else
        super.merge({type: :imageGallery, images: @images.map{|image| image.to_h}})
      end
		end

		def to_md
			if self.images.count == 1
				self.images.first.to_md
			else
				super + "\n" + self.images.map{|x| x.to_md}.join("\n")
			end
		end
	end
end
