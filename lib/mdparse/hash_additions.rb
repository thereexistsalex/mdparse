class Hash
	def keys_to_sym
		self.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
	end
end
