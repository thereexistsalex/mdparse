module MDParse
  class Matcher < ContentList
    ITEM_CLASS = MDParse::Match

    DEFAULTS = {
      value: []
    }

    def to_h
      content = @content.map{|match| match.to_h}.inject([[],[]]){|tot, obj| [tot[0] << obj[0], tot[1] << obj[1]]}
      answer = @content.map{|match| [match.a.options[:uid], match.b.options[:uid]]}
      h = super.merge({contentA: content[0], contentB: content[1], answer: answer})
      h.delete(:content)
      h
    end
  end
end
