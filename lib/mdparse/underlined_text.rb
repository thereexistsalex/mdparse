module MDParse
	class UnderlinedText < Paragraph
		def to_h
			"<u>" + self.content + "</u>"
		end

		def mdparse_original_text
			self.content = MDParse::FormattedText.already_escaped_parse(@original_text)
		end
	end
end
