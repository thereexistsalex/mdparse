module MDParse
	class ChecklistItem < Paragraph
		DEFAULTS = {
			value: false
		}

		def to_md(i=0)
			"+" + super()
		end
	end
end
