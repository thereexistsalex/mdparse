Gem::Specification.new do |s|
  s.name        = 'mdparse'
  s.version     = '7.8.0'
  s.summary     = "Markdown to JSON parser"
  s.description = "A Markdown to JSON parser written for mRendering"
  s.authors     = ["Alex Knapp"]
  s.email       = 'x.knapp@gmail.com'
  s.licenses    = ['private']
  s.files       = Dir.glob("{lib}/**/**/*")

  s.executables << 'mdparse'
end
